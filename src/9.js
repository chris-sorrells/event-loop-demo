const delay = () => {};

//////

[1, 2, 3, 4].forEach(function(i) {
  console.log("do it sync", i);
  delay();
});

function asyncForEach(array, callback) {
  array.forEach(function iterate() {
    setTimeout(callback, 0);
  });
}

asyncForEach([1, 2, 3, 4], function iterate(i) {
  console.log("do it async", i);
  delay();
});
