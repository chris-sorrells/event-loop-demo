let getSync = require("./libs/fetchSync.js");

getSync("https://www.example.com");
getSync("https://www.google.com");
getSync("https://www.gitlab.com");
getSync("https://www.microsoft.com");
getSync("https://www.siemens.com");
