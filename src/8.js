const axios = require("axios"); // use fetch instead

console.log("this message");

axios("https://reqres.in/api/users/2").then(function() {
  return console.log("got a result!");
});

console.log("that message");
