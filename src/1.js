// Call stack example

function multiply(a, b) {
  return a * b;
}

function square(num) {
  return multiply(num, num);
}

function printSquare(num) {
  console.log(square(num));
}

printSquare(5);
