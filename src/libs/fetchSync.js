const axios = require("axios"); // use fetch instead

// don't ever do this please in production code
async function getSync(url) {
  const response = await axios(url);

  await new Promise(resolve => setTimeout(resolve, 3000));

  return response;
}

module.exports = getSync;
