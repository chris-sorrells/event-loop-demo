module.exports = {
  root: true,
  env: {
    node: true,
    es6: true
  },
  extends: ["eslint:recommended", "plugin:prettier/recommended"],
  rules: {
    "no-console": "off"
  },
  parserOptions: {
    parser: "babel-eslint",
    ecmaVersion: 2017
  }
};
